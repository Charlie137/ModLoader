using System.Collections.Generic;
using Serilog;
using Serilog.Events;

namespace Builder;

class BufferedLogger : ILogger
{
    private readonly Queue<LogEvent> _logMessages = new();
    
    public void Write(LogEvent logEvent)
    {
        _logMessages.Enqueue(logEvent);
    }

    /// <summary>
    /// Writes out all the buffered messages to the provided logger
    /// </summary>
    public void WriteToLogger(ILogger logger)
    {
        while (_logMessages.TryDequeue(out var message))
        {
            logger.Write(message);
        }
    }
}