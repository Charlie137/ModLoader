using CommandLine;

namespace Builder.Package;

// ReSharper disable once ClassNeverInstantiated.Global
[Verb("package", HelpText = "Packages the projects and uploads them to Steam")]
class PackageCommandLine : BaseVerb
{
    [Option("skip-uploading", Required = false, Default = false)]
    public bool SkipUploading { get; set; }
    
    [Option("skip-uploader")]
    public bool SkipModUploader { get; set; }
    
    [Option("skip-loader")]
    public bool SkipLoader { get; set; }
    
    [Option("steam-cmd", HelpText = "The path to the file steamcmd.exe")]
    public string SteamCmd { get; set; }
    
    [Option("steam-username")]
    public string SteamUserName { get; set; }
    
    [Option("steam-password")]
    public string SteamPassword { get; set; }
    
    [Option("steam-upload-description")]
    public string SteamUploadDescription { get; set; }
}