using System.Threading.Tasks;
using Serilog;

namespace Builder.Compile;

// ReSharper disable once ClassNeverInstantiated.Global
class CompileAction(ILogger logger)
{

    private const string _publishArguments =
        "--configuration Release --runtime win-x64 --framework net6.0 --self-contained true -consoleLoggerParameters:ErrorsOnly";

    private const string _dotnetBuildArguments = "-p:Configuration=Release -nologo -consoleLoggerParameters:ErrorsOnly";
    public async Task<int> DoAction(CompileCommandLine commandLine)
    {
        logger.Information("Compiling projects");
        
        // NuGet Restore
        var bufferedLogger = new BufferedLogger();
        var result = await Program.Run(bufferedLogger, commandLine.NuGetPath, "restore", commandLine.ModLoaderRepo);
        if (result != 0)
        {
            bufferedLogger.WriteToLogger(logger);
            logger.Error("Failed to NuGet restore");
            return result;
        }
        logger.Information("NuGet restore completed");


        var taskOne = await PublishModLoader(commandLine);
        if (taskOne.exitCode != 0)
        {
            taskOne.logger.WriteToLogger(logger);
            logger.Error("Failed to publish Mod Loader dll");
            return taskOne.exitCode;
        }
        logger.Information("Mod Loader Published");
        
        var taskTwo = await BuildFramework(commandLine);
        
        if (taskTwo.exitCode != 0)
        {
            taskTwo.logger.WriteToLogger(logger);
            logger.Error("Failed to build Mod Loader Framework");
            return taskTwo.exitCode;
        }
        logger.Information("Mod Loader Framework Built");
        
        
        var taskThree = await PublishManager(commandLine);
        if (taskThree.exitCode != 0)
        {
            taskThree.logger.WriteToLogger(logger);
            logger.Error("Failed to publish Mod Manager");
            return taskThree.exitCode;
        }
        logger.Information("Mod Manager Published");
        
        var taskFour = await PublishUploader(commandLine);
        if (taskFour.exitCode != 0)
        {
            taskFour.logger.WriteToLogger(logger);
            logger.Error("Failed to publish Mod Uploader");
            return taskFour.exitCode;
        }
        logger.Information("Mod Uploader Published");

        var taskFive =await  PublishQueries(commandLine);
        if (taskFive.exitCode != 0)
        {
            taskFive.logger.WriteToLogger(logger);
            logger.Error("Failed to publish Steam Queries");
            return taskFive.exitCode;
        }
        logger.Information("Steam Queries Published");

        return 0;
    }

    private async Task<(int exitCode, BufferedLogger logger)> PublishModLoader(CompileCommandLine commandLine)
    {
        var bufferedLogger = new BufferedLogger();
        var exitCode = await Program.Run(bufferedLogger,
            commandLine.DotNetPath,
            $"publish --nologo --configuration Release -consoleLoggerParameters:ErrorsOnly",
            $@"{commandLine.ModLoaderRepo}\ModLoader");
        return (exitCode, bufferedLogger);
    }

    private async Task<(int exitCode, BufferedLogger logger)> BuildFramework(CompileCommandLine commandLine)
    {
        var bufferedLogger = new BufferedLogger();
        var exitCode = await Program.Run(bufferedLogger,
            commandLine.DotNetPath,
            $"build \"Mod Loader.Framework.csproj\" {_dotnetBuildArguments}",
            @$"{commandLine.ModLoaderRepo}\Mod Loader.Framework");
        return (exitCode, bufferedLogger);
    }

    private async Task<(int exitCode, BufferedLogger logger)> PublishManager(CompileCommandLine commandLine)
    {
        var bufferedLogger = new BufferedLogger();
        var exitCode = await Program.Run(bufferedLogger,
            commandLine.DotNetPath,
            $"publish {_publishArguments}",
            $@"{commandLine.ModLoaderRepo}\Mod Manager");
        return (exitCode, bufferedLogger);
    }

    private async Task<(int exitCode, BufferedLogger logger)> PublishUploader(CompileCommandLine commandLine)
    {
        var bufferedLogger = new BufferedLogger();
        var exitCode = await Program.Run(bufferedLogger,
            commandLine.DotNetPath,
            $"publish {_publishArguments}",
            $@"{commandLine.ModLoaderRepo}\Mod Uploader");
        return (exitCode, bufferedLogger);
    }

    private async Task<(int exitCode, BufferedLogger logger)> PublishQueries(CompileCommandLine commandLine)
    {
        var bufferedLogger = new BufferedLogger();
        var exitCode = await Program.Run(bufferedLogger,
            commandLine.DotNetPath,
            $"publish {_publishArguments.Replace("net6.0", "net7.0")}",
            $@"{commandLine.ModLoaderRepo}\SteamQueries");
        return (exitCode, bufferedLogger);
    }
}