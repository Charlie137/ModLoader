using System.Diagnostics;
using System.IO.Abstractions.TestingHelpers;
using System.Runtime.InteropServices;
using Facepunch.Steamworks.Abstractions;
using Microsoft.Reactive.Testing;
using Mod_Manager.Abstractions;
using Mod_Manager.Abstractions.VIewModel;
using Mod_Manager.ViewModels;
using ReactiveUI.Testing;

namespace Mod_Manager.Tests.ViewModels;

public class HeaderBarViewModelTests
{
    [Category("Unit")]
    public class a_header_bar
    {
        [Test]
        public void when_pressing_help_opens_docs() => new TestScheduler().With(_ =>
        {
            var http = new Mock<IHttp>();
            
            var view = new HeaderBarViewModel(Mock.Of<ILogsZipGenerator>(),
                Mock.Of<IRuntimeInfo>(),
                Mock.Of<IProcess>(), 
                http.Object, 
                Mock.Of<IPopUpView>(), 
                Mock.Of<IHomeViewModel>(), 
                Mock.Of<IQuery>(),
                new MockFileSystem(),
                Mock.Of<ISettings>(),
                Mock.Of<IInit>());

            view.HelpCommand.Execute().Subscribe();
            
            http.Verify(h => h.OpenUrl("https://vtolvr-mods.com/docs/getting-started/"), Times.Once);
        });

        [Category("Unit")]
        class when_launching
        {
            [Test]
            public void starts_process_and_watches_it() => new TestScheduler().With(_ =>
            {
                var process = new Mock<IProcess>();
                var fakeVtolVR = new Process();
                process.Setup(p => p.StartVtolVr())
                    .ReturnsAsync(fakeVtolVR);

                var view = new HeaderBarViewModel(Mock.Of<ILogsZipGenerator>(), Mock.Of<IRuntimeInfo>(),
                    process.Object, Mock.Of<IHttp>(), Mock.Of<IPopUpView>(), 
                    Mock.Of<IHomeViewModel>(), 
                    Mock.Of<IQuery>(),
                    new MockFileSystem(),
                    Mock.Of<ISettings>(),
                    Mock.Of<IInit>());

                view.LaunchCommand.Execute().Subscribe();
                
                process.Verify(p => p.StartVtolVr(), Times.Once);
                view.CanLaunchGame.Should().BeFalse();
                fakeVtolVR.EnableRaisingEvents.Should().BeTrue();
            });

            [Test]
            public void does_not_lock_ui_if_process_not_found() => new TestScheduler().With(_ =>
            {
                var process = new Mock<IProcess>();
                var view = new HeaderBarViewModel(Mock.Of<ILogsZipGenerator>(), Mock.Of<IRuntimeInfo>(),
                    process.Object, Mock.Of<IHttp>(), Mock.Of<IPopUpView>(), 
                    Mock.Of<IHomeViewModel>(), 
                    Mock.Of<IQuery>(),
                    new MockFileSystem(),
                    Mock.Of<ISettings>(),
                    Mock.Of<IInit>());
                
                view.LaunchCommand.Execute().Subscribe();
                process.Verify(p => p.StartVtolVr(), Times.Once);
                view.CanLaunchGame.Should().BeTrue();
            });
        }

        [Category("Unit")]
        class when_creating_logs
        {
            [Test]
            public void checks_if_vtol_vr_is_running() => new TestScheduler().With(_ =>
            {
                var process = new Mock<IProcess>();
                process.Setup(p => p.GetProcessesByName("vtolvr"))
                    .Returns(new Process[1]);

                var popUp = new Mock<IPopUpView>();
                
                var view = new HeaderBarViewModel(Mock.Of<ILogsZipGenerator>(),
                    Mock.Of<IRuntimeInfo>(),
                    process.Object, Mock.Of<IHttp>(), popUp.Object, 
                    Mock.Of<IHomeViewModel>(), 
                    Mock.Of<IQuery>(),
                    new MockFileSystem(),
                    Mock.Of<ISettings>(),
                    Mock.Of<IInit>());

                view.CollectLogsCommand.Execute().Subscribe();

                popUp.Verify(
                    p => p.ShowPopUp(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string?>(), It.IsAny<string?>()),
                    Times.Once);
            });

            [Test]
            public void calls_zip_generator()
            {
                var process = new Mock<IProcess>();
                process.Setup(p => p.GetProcessesByName("vtolvr"))
                    .Returns(new Process[0]);

                var zipGenerator = new Mock<ILogsZipGenerator>();
                var runtimeInfo = new Mock<IRuntimeInfo>();
                runtimeInfo.Setup(r => r.IsOSPlatform(OSPlatform.Windows))
                    .Returns(true);
                
                var view = new HeaderBarViewModel(zipGenerator.Object,
                    runtimeInfo.Object,
                    process.Object, Mock.Of<IHttp>(), Mock.Of<IPopUpView>(), 
                    Mock.Of<IHomeViewModel>(), 
                    Mock.Of<IQuery>(),
                    new MockFileSystem(),
                    Mock.Of<ISettings>(),
                    Mock.Of<IInit>());

                view.CollectLogsCommand.Execute().Subscribe();
                
                zipGenerator.Verify(z => z.CollectLogs(), Times.Once);
                process.Verify(p => p.Start("explorer.exe", It.IsAny<string>()), Times.Once);

            }
        }

        [Category("Unit")]
        class when_opening_workshop_folder
        {
            [Test]
            public void if_linux_shows_a_pop_up_to_the_user() => new TestScheduler().With(_ =>
            {
                const string anypath = "AnyPath";
                
                var popUp = new Mock<IPopUpView>();
                var process = new Mock<IProcess>();
                var init = new Mock<IInit>();
                init.Setup(i => i.GetWorkshopFolder()).Returns(anypath);
                var runtimeInfo = new Mock<IRuntimeInfo>();
                runtimeInfo.Setup(r => r.IsOSPlatform(OSPlatform.Windows)).Returns(false);
                
                var view = new HeaderBarViewModel(Mock.Of<ILogsZipGenerator>(),
                    runtimeInfo.Object,
                    process.Object, 
                    Mock.Of<IHttp>(), 
                    popUp.Object, 
                    Mock.Of<IHomeViewModel>(), 
                    Mock.Of<IQuery>(),
                    new MockFileSystem(),
                    Mock.Of<ISettings>(),
                    init.Object);

                view.OpenWorkshopFolderCommand.Execute().Subscribe();
                popUp.Verify(
                    p => p.ShowPopUp("Expected folder path", anypath, It.IsAny<string?>(), It.IsAny<string?>()), Times.Once);
            });
            
            [Test]
            public void if_windows_opens_explorer() => new TestScheduler().With(_ =>
            {
                const string anypath = "AnyPath";
                
                var process = new Mock<IProcess>();
                var init = new Mock<IInit>();
                init.Setup(i => i.GetWorkshopFolder()).Returns(anypath);
                var runtimeInfo = new Mock<IRuntimeInfo>();
                runtimeInfo.Setup(r => r.IsOSPlatform(OSPlatform.Windows)).Returns(true);
                
                var view = new HeaderBarViewModel(Mock.Of<ILogsZipGenerator>(),
                    runtimeInfo.Object,
                    process.Object, 
                    Mock.Of<IHttp>(), 
                    Mock.Of<IPopUpView>(), 
                    Mock.Of<IHomeViewModel>(), 
                    Mock.Of<IQuery>(),
                    new MockFileSystem(),
                    Mock.Of<ISettings>(),
                    init.Object);

                view.OpenWorkshopFolderCommand.Execute().Subscribe();
                
                process.Verify(p => p.Start("explorer.exe", It.IsAny<string>()), Times.Once);
            });
            
            [Test]
            public void if_can_not_find_path_tells_user() => new TestScheduler().With(_ =>
            {
                const string anypath = "AnyPath";
                var popup = new Mock<IPopUpView>();
                var init = new Mock<IInit>();
                init.Setup(i => i.GetWorkshopFolder()).Returns(string.Empty);
                
                var view = new HeaderBarViewModel(Mock.Of<ILogsZipGenerator>(),
                    Mock.Of<IRuntimeInfo>(),
                    Mock.Of<IProcess>(), 
                    Mock.Of<IHttp>(), 
                    popup.Object, 
                    Mock.Of<IHomeViewModel>(), 
                    Mock.Of<IQuery>(),
                    new MockFileSystem(),
                    Mock.Of<ISettings>(),
                    init.Object);

                view.OpenWorkshopFolderCommand.Execute().Subscribe();

                popup.Verify(
                    p => p.ShowPopUp("Failed", "Failed to find the path of the workshop folder.", It.IsAny<string?>(),
                        It.IsAny<string?>()), Times.Once);
            });
        }
    }
}