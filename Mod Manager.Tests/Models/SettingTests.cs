﻿using Facepunch.Steamworks.Abstractions;
using Mod_Manager.Models;

namespace Mod_Manager.Tests.Models;

public class SettingTests
{
    private const string fileName = "settings.json";

    [Test]
    public void can_set_vtol_path_without_prior_settings()
    {
        const string fakeVtolDir = @"C:\Program Files (x86)\Steam\steamapps\common\VTOL VR";
        var fakeSteamStorage = new Mock<ISteamRemoteStorage>();
        
        fakeSteamStorage
            .Setup(instance => instance.FileExists(fakeVtolDir))
            .Returns(false);
        fakeSteamStorage
            .Setup(instance => instance.FileWrite(fileName, It.IsAny<string>()))
            .Returns(true);
        
        var settings = new Mod_Manager.Models.Settings(fakeSteamStorage.Object);
        settings.SetVRMode(Settings.VRMode.OpenXR);
    }

    [Test]
    public void can_get_vtol_from_cloud()
    {
        var expected = Settings.VRMode.OpenXR;
        string jsonText = "{ \"VRMode\": \"" + expected + "\" }";
        var fakeSteamStorage = new Mock<ISteamRemoteStorage>();

        fakeSteamStorage
            .Setup(instance => instance.FileExists(fileName))
            .Returns(true);
        fakeSteamStorage
            .Setup(instance => instance.FileReadText(fileName))
            .Returns(jsonText);
        
        var settings = new Mod_Manager.Models.Settings(fakeSteamStorage.Object);
        settings.GetVRMode().Should().Be(Settings.VRMode.OpenXR);
    }
}