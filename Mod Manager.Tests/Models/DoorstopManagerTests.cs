﻿using System.IO.Abstractions.TestingHelpers;
using Mod_Manager.Abstractions;
using Mod_Manager.Abstractions.ConfigParser;
using Mod_Manager.Models;
using Path = System.IO.Path;

namespace Mod_Manager.Tests.Models;

public class DoorstopManagerTests
{
    [Category("Unit")]
    class a_doorstop_mananger_when_checking_for_old_file
    {

        [Test]
        public void creates_default_one_if_old_one_exists()
        {
            const string vtolVrDir = @"D:\Steam\steamapps\common\VTOL VR";
            
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { Path.Combine(vtolVrDir, "doorstop_config.ini"), OldConfigFileContents }
            });
            
            var fileManager = new Mock<IFileManager>();
            fileManager.Setup(fm => fm.GetVtolDirectory())
                .Returns(vtolVrDir);
            
            var configParser = new Mock<IConfigParser>();
            configParser.Setup(c => c.SectionExists("UnityDoorstop"))
                .Returns(true);

            var manager = new DoorstopManager(fileSystem, fileManager.Object, configParser.Object);
            manager.CheckForOldFile();

            fileSystem.File.ReadAllText(Path.Combine(vtolVrDir, "doorstop_config.ini")).Should()
                .BeEquivalentTo(DefaultConfigContents);
        }
        
        [Test]
        public void creates_default_one_if_nothing_is_found()
        {
            const string vtolVrDir = @"D:\Steam\steamapps\common\VTOL VR";
            
            var fileSystem = new MockFileSystem();
            fileSystem.Directory.CreateDirectory(vtolVrDir);
            
            var fileManager = new Mock<IFileManager>();
            fileManager.Setup(fm => fm.GetVtolDirectory())
                .Returns(vtolVrDir);

            var manager = new DoorstopManager(fileSystem, fileManager.Object, Mock.Of<IConfigParser>());
            manager.CheckForOldFile();

            fileSystem.FileExists(Path.Combine(vtolVrDir, "doorstop_config.ini")).Should().BeTrue();
        }
        
        [Test]
        public void changes_it_to_false_if_true()
        {
            const string vtolVrDir = @"D:\Steam\steamapps\common\VTOL VR";
            
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { Path.Combine(vtolVrDir, "doorstop_config.ini"), DefaultConfigContents.Replace("enabled=false", "enabled=true") }
            });
            
            var fileManager = new Mock<IFileManager>();
            fileManager.Setup(fm => fm.GetVtolDirectory())
                .Returns(vtolVrDir);
            
            var configParser = new Mock<IConfigParser>();
            configParser.Setup(c => c.SectionExists("UnityDoorstop"))
                .Returns(false);
            configParser.Setup(c => c.SectionExists("General"))
                .Returns(true);
            configParser.Setup(c => c.GetValue(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>()))
                .Returns(true);

            var manager = new DoorstopManager(fileSystem, fileManager.Object, configParser.Object);
            manager.CheckForOldFile();
            
            configParser.Verify(c => c.SetValue("General", "enabled", false), Times.Once);
            configParser.Verify(c => c.Save(), Times.Once);
        }

        [Test]
        public void deletes_it_if_vtpatcher_is_present()
        {
            const string vtolVrDir = @"D:\Steam\steamapps\common\VTOL VR";
            
            var fileSystem = new MockFileSystem(new Dictionary<string, MockFileData>
            {
                { Path.Combine(vtolVrDir, "doorstop_config.ini"), OldDoorstopFourConfigFileContents }
            });
            
            var fileManager = new Mock<IFileManager>();
            fileManager.Setup(fm => fm.GetVtolDirectory())
                .Returns(vtolVrDir);
            
            var configParser = new Mock<IConfigParser>();
            configParser.Setup(c => c.SectionExists("UnityDoorstop"))
                .Returns(false);
            configParser.Setup(c => c.SectionExists("General"))
                .Returns(true);
            configParser.Setup(c => c.GetValue(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                .Returns(@"VTOLVR_ModLoader\VTPatcher.dll");

            var manager = new DoorstopManager(fileSystem, fileManager.Object, configParser.Object);
            manager.CheckForOldFile();

            var text = fileSystem.File.ReadAllText(Path.Combine(vtolVrDir, "doorstop_config.ini"));
            text.Should().BeEquivalentTo(DefaultConfigContents);
        }
    }
    public const string DefaultConfigContents = @"# General options for Unity Doorstop
[General]

# Enable Doorstop?
enabled=false

# Path to the assembly to load and execute
# NOTE: The entrypoint must be of format `static void Doorstop.Entrypoint.Start()`
target_assembly=@Mod Loader\Managed\Mod Loader.dll

# If true, Unity's output log is redirected to <current folder>\output_log.txt
redirect_output_log=false

# If enabled, DOORSTOP_DISABLE env var value is ignored
# USE THIS ONLY WHEN ASKED TO OR YOU KNOW WHAT THIS MEANS
ignore_disable_switch=false


# Options specific to running under Unity Mono runtime
[UnityMono]

# Overrides default Mono DLL search path
# Sometimes it is needed to instruct Mono to seek its assemblies from a different path
# (e.g. mscorlib is stripped in original game)
# This option causes Mono to seek mscorlib and core libraries from a different folder before Managed
# Original Managed folder is added as a secondary folder in the search path
dll_search_path_override=@Mod Loader\Managed

# If true, Mono debugger server will be enabled
debug_enabled=false

# When debug_enabled is true, specifies the address to use for the debugger server
debug_address=127.0.0.1:10000

# If true and debug_enabled is true, Mono debugger server will suspend the game execution until a debugger is attached
debug_suspend=false

# Options sepcific to running under Il2Cpp runtime
[Il2Cpp]

# Path to coreclr.dll that contains the CoreCLR runtime
coreclr_path=

# Path to the directory containing the managed core libraries for CoreCLR (mscorlib, System, etc.)
corlib_dir=";
    
    public const string OldConfigFileContents = @"[UnityDoorstop]
# Specifies whether assembly executing is enabled
enabled=true
# Specifies the path (absolute, or relative to the game's exe) to the DLL/EXE that should be executed by Doorstop
targetAssembly=Doorstop.dll
# Specifies whether Unity's output log should be redirected to <current folder>\output_log.txt
redirectOutputLog=false
# If enabled, DOORSTOP_DISABLE env var value is ignored
# USE THIS ONLY WHEN ASKED TO OR YOU KNOW WHAT THIS MEANS
ignoreDisableSwitch=false
# Overrides default Mono DLL search path
# Sometimes it is needed to instruct Mono to seek its assemblies from a different path
# (e.g. mscorlib is stripped in original game)
# This option causes Mono to seek mscorlib and core libraries from a different folder before Managed
# Original Managed folder is added as a secondary folder in the search path
dllSearchPathOverride=


# Settings related to bootstrapping a custom Mono runtime
# Do not use this in managed games!
# These options are intended running custom mono in IL2CPP games!
[MonoBackend]
# Path to main mono.dll runtime library
runtimeLib=
# Path to mono config/etc directory
configDir=
# Path to core managed assemblies (mscorlib et al.) directory
corlibDir=";

    public const string OldDoorstopFourConfigFileContents = @"# General options for Unity Doorstop
[General]

# Enable Doorstop?
enabled=false

# Path to the assembly to load and execute
# NOTE: The entrypoint must be of format `static void Doorstop.Entrypoint.Start()`
target_assembly=VTOLVR_ModLoader\VTPatcher.dll

# If true, Unity's output log is redirected to <current folder>\output_log.txt
redirect_output_log=false

# If enabled, DOORSTOP_DISABLE env var value is ignored
# USE THIS ONLY WHEN ASKED TO OR YOU KNOW WHAT THIS MEANS
ignore_disable_switch=false


# Options specific to running under Unity Mono runtime
[UnityMono]

# Overrides default Mono DLL search path
# Sometimes it is needed to instruct Mono to seek its assemblies from a different path
# (e.g. mscorlib is stripped in original game)
# This option causes Mono to seek mscorlib and core libraries from a different folder before Managed
# Original Managed folder is added as a secondary folder in the search path
dll_search_path_override=

# If true, Mono debugger server will be enabled
debug_enabled=false

# When debug_enabled is true, specifies the address to use for the debugger server
debug_address=127.0.0.1:10000

# If true and debug_enabled is true, Mono debugger server will suspend the game execution until a debugger is attached
debug_suspend=false

# Options sepcific to running under Il2Cpp runtime
[Il2Cpp]

# Path to coreclr.dll that contains the CoreCLR runtime
coreclr_path=

# Path to the directory containing the managed core libraries for CoreCLR (mscorlib, System, etc.)
corlib_dir=";
}