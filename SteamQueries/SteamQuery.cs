﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Serilog;
using SimpleTCP;
using SteamQueries.Models;
using Steamworks;
using Steamworks.Ugc;

namespace SteamQueries;

internal class SteamQuery
{
    private readonly int _port;
    private readonly uint _appId;
    private bool _initialised;
    private SimpleTcpServer _tcpServer;
    private ulong _nextMessageId = 0;
    private readonly object _idLock = new ();
    private readonly TimeSpan _messageBuffer;
    private readonly Queue<(Message message, IMessage response)> _messageQueue = new();
    
    public SteamQuery(int port, uint appId)
    {
        _port = port;
        _appId = appId;
    }

    public async Task<int> Init()
    {
        if (_initialised)
        {
            Log.Warning("Steam Query has already been initialised on port {Port} for app id {AppId}", _port, _appId);
            return -1;
        }

        try
        {
            _tcpServer = new SimpleTcpServer();
            _tcpServer.DataReceived += (async (sender, message) => await OnMessageEvent(sender, message));
            _tcpServer.Start(_port);
        }
        catch (Exception e)
        {
            Log.Error("Failed to create TCP Server on port {Port}. {ExceptionMessage}", _port, e.Message);
            return -1;
        }
        
        Log.Information("Connected to game on port {Port}", _port);
        

        try
        {
            SteamClient.Init(_appId);
            _initialised = true;
        }
        catch (Exception e)
        {
            Log.Error("Error when starting steamworks:\n{Error}", e.ToString());
            return -1;
        }
        
        Log.Information("Started Steamworks with app id of {AppId}", _appId);
        return 0;
    }

    private async Task OnMessageEvent(object sender, Message e)
    {
        var messageChopped = e.MessageString.Remove(e.MessageString.Length - 1);
        Log.Information("Raw message received '{Message}'", messageChopped);
        var message = default(IMessage);
        try
        {
            message = JsonConvert.DeserializeObject<IMessage>(messageChopped);
        }
        catch (Exception exception)
        {
            Log.Error(exception, "Error when trying to read message");
            Log.Information("Message Contents\n{MessageText}", messageChopped);
            return;
        }
        try
        {
            switch (message)
            {
                case GetSubscribedItemsRequest request:
                    await GetSubscribedItems(request, e);
                    break;
                case GetItemRequest request:
                    await GetItem(request, e);
                    break;
                case LoadOnStartRequest:
                    GetLoadOnStartSettings(e);
                    break;
                case DownloadRequest request:
                    await StartDownload(request, e);
                    break;
                default:
                    Log.Warning("The type {Type} wasn't handled for", message.GetType().FullName);
                    break;
            }
        }
        catch (Exception exception)
        {
            Log.Fatal(exception, "FAILED TO PROCESS REQUEST!");
        }
    }

    private ulong GetNextId()
    {
        ulong nextId = 0;
        lock (_idLock)
        {
            nextId = _nextMessageId;
            _nextMessageId++;
        }

        return nextId;
    }

    public async Task Tick()
    {
        var delay = TimeSpan.FromSeconds(1);
        while (_tcpServer.IsStarted)
        {
            await Task.Delay(delay);
            if (_messageQueue.TryDequeue(out var pendingMessage))
            {
                pendingMessage.message.Reply(pendingMessage.response);
            }
        }
        Log.Information("Shutting down");
        SteamClient.Shutdown();
    }

    private async Task GetSubscribedItems(GetSubscribedItemsRequest request, Message message)
    {
        Log.Information("Received request to get user's subscribed items on page {Page}", request.Page);
        var query = Query.Items.WhereUserSubscribed().WithLongDescription(true).WithMetadata(true).WithChildren(true).WithDifferentApp(3018410, 3018440);
        var results = await query.GetPageAsync(request.Page);
        
        Log.Information("Sending response for {MethodName}", nameof(GetSubscribedItems));
        SendMessageBuffered(message, new GetSubscribedItemsResponse
        {
            HasValues = results.HasValue,
            Items = results.Value.Entries.ToModel()
        });
    }

    private async Task GetItem(GetItemRequest request, Message message)
    {
        Log.Information("Received request to get item details for {PublishFieldId}", request.PublishFieldId);
        var query = Query.Items.WithFileId(request.PublishFieldId).WithLongDescription(true).WithMetadata(true).WithChildren(true).WithDifferentApp(3018410, 3018440);
        var results = await query.GetPageAsync(1);
        
        Log.Information("Sending response for {MethodName}", nameof(GetItem));
        SendMessageBuffered(message, new GetItemResponse
        {
            HasValues = results.HasValue,
            Items = results.Value.Entries.ToModel()
        });
    }

    private void GetLoadOnStartSettings(Message message)
    {
        Log.Information("Received request to get Load On Start Settings");
        var bytes = SteamRemoteStorage.FileRead("Load on Start");
        var text = Encoding.ASCII.GetString(bytes);
        var type = JsonConvert.DeserializeObject<LoadOnStart>(text);
        
        Log.Information("Sending Response for {MethodName}", nameof(GetLoadOnStartSettings));
        SendMessageBuffered(message, new LoadOnStartResponse { Data = type });
    }

    private async Task StartDownload(DownloadRequest request, Message message)
    {
        var messageId = GetNextId();

        SendMessageBuffered(message, new DownloadResponse { MessageId = messageId });
        
        var item = new Item(request.PublishFieldId);
        await item.Subscribe();

        await item.DownloadAsync(f => SendMessageBuffered(message, new DownloadUpdate { MessageId = messageId, Progress = f * 100 }));

        SendMessageBuffered(message, new DownloadComplete { MessageId = messageId });
    }
    
    /// This is a horrible hack to stop the game receiving two messages in one data received
    private void SendMessageBuffered(Message message, IMessage response)
    {
        _messageQueue.Enqueue((message, response));
    }
}