﻿using CommandLine;

namespace SteamQueries;

internal class CommandLineOptions
{
    [Option('p', "port", Required = true, HelpText = "Set the port for communication over TCP")]
    public int Port { get; set; }
    
    [Option('a', "appId", Required = true, HelpText = "The app id to query")]
    public int AppId { get; set; }
}