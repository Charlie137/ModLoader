﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Serilog;
using SimpleTCP;
using SteamQueries.Models;
using Steamworks.Ugc;

namespace SteamQueries;

internal static class Extensions
{
    public static string Print(this string[] array)
    {
        var builder = new StringBuilder("[");

        for (int i = 0; i < array.Length; i++)
        {
            string s = array[i];
            builder.Append(s);
            if (i != array.Length - 1)
            {
                builder.Append(',');
            }
        }

        builder.Append(']');

        return builder.ToString();
    }

    public static List<SteamItem> ToModel(this IEnumerable<Item> items)
    {
        Log.Debug("Converting FacePunch Item list to our SteamItem list");
        var response = new List<SteamItem>();
        foreach (var item in items)
        {
            var itemMetaData = (ItemMetaData)default;

            try
            {
                itemMetaData = JsonConvert.DeserializeObject<ItemMetaData>(item.Metadata);
            }
            catch (Exception e)
            {
                Log.Error(e,"Failed to deserialize object from MetaData for workshop item '{ItemName}'. Raw Metadata={MetaData}", item.Title, item.Metadata);
            }

            var steamItem = new SteamItem
            {
                Title = item.Title,
                Description = item.Description,
                Tags = item.Tags,
                PreviewImageUrl = item.PreviewImageUrl,
                NumSubscriptions = item.NumSubscriptions,
                Owner = new User
                {
                    Name = item.Owner.Name,
                    SteamId = item.Owner.Id.Value
                },
                PublishFieldId = item.Id.Value,
                MetaData = itemMetaData,
                Directory = item.Directory,
                DependenciesIds = item.Children?.Select(child => child.Value).ToArray(),
                IsInstalled = item.IsInstalled
            };
            Log.Debug("Converted Item:{STEAMITEM}", steamItem.ToString());
            response.Add(steamItem);
        }

        return response;
    }
    
    public static void Reply(this Message message, IMessage response)
    {
        var responseText = JsonConvert.SerializeObject(response, Formatting.None,
            new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All });
        message.Reply(responseText);
        Log.Information("Sending {ResponseText}", responseText);
    }
}