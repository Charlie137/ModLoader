namespace SteamQueries.Models
{
    public class LoadOnStartRequest : IMessage
    {
        public string MessageType { get; set; } = nameof(LoadOnStartRequest);
    }
}