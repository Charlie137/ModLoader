namespace SteamQueries.Models
{
    public class DownloadResponse : IMessage
    {
        public string MessageType { get; set; } = nameof(DownloadResponse);
        public ulong MessageId { get; set; }
    }
}