namespace SteamQueries.Models
{
    public class DownloadUpdate : IMessage
    {
        public string MessageType { get; set; } = nameof(DownloadUpdate);
        public ulong MessageId { get; set; }
        public float Progress { get; set; }
    }
}