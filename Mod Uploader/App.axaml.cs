using System;
using System.IO.Abstractions;
using System.Reactive;
using System.Reactive.Concurrency;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using Facepunch.Steamworks.Abstractions;
using Facepunch.Steamworks.Abstractions.Wrappers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Mod_Uploader.Abstractions;
using Mod_Uploader.Models;
using Mod_Uploader.Utilities;
using Mod_Uploader.ViewModels;
using Mod_Uploader.Views;
using ReactiveUI;
using Serilog;
using Serilog.Formatting.Compact;
using Steamworks;

namespace Mod_Uploader;

public partial class App : Application
{
    public static uint UploaderAppId = 3018440;
    public static uint LoaderAppId = 3018410;
    public const string LogsDirectoryName = "Logs";
    
    public override void Initialize()
    {
        AvaloniaXamlLoader.Load(this);
    }

    public override async void OnFrameworkInitializationCompleted()
    {
        RxApp.DefaultExceptionHandler = Observer.Create<Exception>(e =>
        {
            Log.Error(e, "An exception has thrown");
        });
        
        var builder = Host.CreateApplicationBuilder();

        builder.Services.AddSingleton<IContext, Context>();
        builder.Services.AddSingleton<IProcess, Process>();
        builder.Services.AddSingleton<IFileSystem, FileSystem>();
        builder.Services.AddSingleton<IQuery>(new QueryWrapper(UgcType.Items));
        builder.Services.AddSingleton<IEditorFactory>(new EditorFactory());
        builder.Services.AddSingleton<IMD5>(new MD5Wrapped());
        
        builder.Services.AddSingleton<IMainWindowViewModel, MainWindowViewModel>();
        builder.Services.AddSingleton<IFormViewModel, FormViewModel>();
        builder.Services.AddSingleton<IHeaderViewModel, HeaderViewModel>();
        builder.Services.AddSingleton<IScheduler>(RxApp.MainThreadScheduler);
        builder.Services.AddSingleton<IPopUpView, PopUpViewModel>();
        
        using var host = builder.Build();
        var services = host.Services;
        
        CreateLogger(services.GetService<IFileSystem>());
        
        StartSteam();
        
        var mainWindow = services.GetService<IMainWindowViewModel>();
        var headerView = services.GetService<IHeaderViewModel>();
        
        headerView.UpdateTitleText();
        
        // This is a long operation asking steam for the users workshop items
        // Don't want to hold up the UI while we do that
        _ = Task.Run(headerView.FetchUsersItems);
        
        if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
        {
            
            desktop.MainWindow = new MainWindow { DataContext = mainWindow };
        }

        base.OnFrameworkInitializationCompleted();
    }
    
    private static void StartSteam()
    {
        Log.Information("Starting Steam");
        try
        {
            SteamClient.Init(UploaderAppId);
        }
        catch (Exception e)
        {
            // Catching error in Rider previewer
        }
    }
    
    private static void CreateLogger(IFileSystem fileSystem)
    {
        var currentTime = DateTime.Now;
        var logPath = LogsDirectoryName + $"/Log_{currentTime.Ticks}.txt";
            
        Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Debug()
            .WriteTo.Console()
            .WriteTo.File(new CompactJsonFormatter(), logPath, shared:true)
            .CreateLogger();

        var currentDir = fileSystem.Directory.GetCurrentDirectory();
        Log.Information("Program Started in '{CURRENTFOLDER}'", currentDir);
    }
}