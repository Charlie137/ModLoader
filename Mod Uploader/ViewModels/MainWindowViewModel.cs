﻿using System.IO.Abstractions.TestingHelpers;
using Facepunch.Steamworks.Abstractions.Wrappers;
using Mod_Uploader.Abstractions;
using Mod_Uploader.Models;
using Mod_Uploader.Utilities;

namespace Mod_Uploader.ViewModels;

public class MainWindowViewModel : ViewModelBase, IMainWindowViewModel
{
    private readonly IViewModel _headerView;
    private readonly IViewModel _formView;
    private readonly IViewModel _popUpView;
    
    public ViewModelBase HeaderBarView => _headerView.GetViewModel();
    public ViewModelBase FormView => _formView.GetViewModel();
    public ViewModelBase PopUpView => _popUpView.GetViewModel();

    public MainWindowViewModel(IHeaderViewModel headerViewModel, IFormViewModel formViewModel, IPopUpView popUpView)
    {
        _headerView = headerViewModel;
        _formView = formViewModel;
        _popUpView = popUpView;
    }

    /// <summary>
    /// XAML Preview
    /// </summary>
    public MainWindowViewModel() : this(new HeaderViewModel(),
        new FormViewModel(new Context(),
            new MockFileSystem(),
            new EditorFactory(),
            new MD5Wrapped(),
            null,
            new Process()),
        new PopUpViewModel())
    {
    }


    public ViewModelBase GetViewModel() => this;
}