using System;
using System.Reactive;
using ReactiveUI;
using Serilog;

namespace Mod_Uploader.Models;

public class WorkshopTag
{
    public string Name { get; set; }
    public bool IsChecked { get; set; }
    public ReactiveCommand<Unit, Unit> OnSelectedChangedCommand { get; set; }

    private Action<WorkshopTag, bool> IsCheckedChanged;
    
    public WorkshopTag()
    {
        OnSelectedChangedCommand = ReactiveCommand.Create(InvokeIsCheckedChanged);
    }

    public WorkshopTag(string name, Action<WorkshopTag, bool> isCheckedChanged) : this()
    {
        IsCheckedChanged = isCheckedChanged;
        Name = name;
    }


    private void InvokeIsCheckedChanged()
    {
        if (IsChecked)
        {
            Log.Information("User selected the tag {WorkshopTag}", Name); 
        }
        else
        {
            Log.Information("User deselected the tag {WorkshopTag}", Name);
        }
        
        IsCheckedChanged?.Invoke(this, IsChecked);
    }
}