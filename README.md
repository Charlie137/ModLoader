# VTOL VR Mod Loader

- [Steam Store Page](https://store.steampowered.com/app/3018410/VTOL_VR_Mod_Loader/)
- [Youtube](https://www.youtube.com/watch?v=bTguACQmxeE)

[![Watch the video](https://img.youtube.com/vi/bTguACQmxeE/maxresdefault.jpg)](https://youtu.be/bTguACQmxeE)

*Trailer on YouTube*


VTOL VR Modding aims to add more user-created content into the game as mods. With the mod loader, players can add custom code into the game to add extra features that they wanted.

## [Creating a mod](https://docs.vtolvr-mods.com/creating-a-mod/index.html "Guide on creating a mod")

If you just want to create a mod for the mod loader. You can follow the guide at [vtolvr-mods.com](https://docs.vtolvr-mods.com/creating-a-mod/index.html "Guide on how to create a mod") to get started with creating mods. To create a mod it requires some basic knowledge how [Unity](https://unity.com/ "Unity Game Engine") game engine works and C# but people have still managed to learn it on the go.

# Building

These projects should be a mixture of .NET 6, .NET 7, .NET 8, .NET Standard 2.0. So using the dotnet build should build the whole solution as long as you have those .NET SDKs

# Contents of this repository

## Builder

This project is what builds and uploads the mod loader to steam. Similar to a pipeline but ran locally.

## [Facepunch.Steamworks.Abstractions](https://gitlab.com/ben-w/facepunch.steamworks.abstractions)

This project makes Facepunch Steamworks testable, it is referenced directly through a git submodule because it is not pushed to a NuGet feed.

## [Facepunch.Steamworks.Win64](https://github.com/MarshMello0/Facepunch.Steamworks)

This is a fork of Facepunch Steamworks because these PRs not being merged yet: [!762](https://github.com/Facepunch/Facepunch.Steamworks/pull/762) and [!759](https://github.com/Facepunch/Facepunch.Steamworks/pull/759).
This library is used to interact with Steam, however regrettably the main Facepunch repo is inactive so having to maintain this fork.

## [JsonSubTypes](https://gitlab.com/vtolvr-mods/JsonSubTypes)

Json Sub Types is used for the communication between Steam Queires project and the Mod Loader project. These projects share the types from SteamQueries.Models, so this library helps tell when sending a message over TCP what type it is.

The fork was created just it needed .NET 7 support.

## Mod Loader

The Mod Loader project is all the in game code, this is a class library loaded by [Doorstop](https://github.com/NeighTools/UnityDoorstop) on boot of the game. 

## Mod Loader.Framework

A small class library which has the `VtolMod` mod developers use in their projects. This saves them having to pull in all the games code and extra packages just to create a simple mod.

## Mod Manager

This is the main application which launches when you press play on Steam. It is created using [Avalonia UI](https://avaloniaui.net/), it has tests under the project Mod Manager.Tests.

## Mod Uploader

The uploader is what mod developers user to upload and update their mods on the workshop. It is a separate tool which is also created in [Avalonia UI](https://avaloniaui.net/).

## SteamQueries

Steam Queries was created because Steam only have one application per process. So this console application runs in the background when players launch the game, then the game code calls TCP requests to it to make requests to Steam for it.

## SteamQueries.Models

These are the types that are shared between the Mod Loader and Steam Queries, they are passed over TCP and use JsonSubTypes for telling what type is being sent.

# Contributors

A special thanks to all these people for their help in creating the mod loader to what it is today.

[Ketkev](https://github.com/ketkev "Ketkev's Github") for all his work on the website, hosting the website, maintaining the website and assistant with managing the project.

[Nebriv](https://github.com/nebriv "Nebriv's Github") for his early support to the mod loader, help with bug testing and help with setting up the new website.

[Temperz87](https://gitlab.com/Temperz87) for minor bug fixed in [different pull request](https://gitlab.com/vtolvr-mods/ModLoader/-/merge_requests?scope=all&state=merged&author_username=Temperz87)

[Yellowbluesky](https://gitlab.com/yellowbluesky) for a typo in pull request [!102](https://gitlab.com/vtolvr-mods/ModLoader/-/merge_requests/102)

[sgoodwin3105](https://gitlab.com/sgoodwin3105) for fixing a bug with skins in pull request [!83](https://gitlab.com/vtolvr-mods/ModLoader/-/merge_requests/83)
