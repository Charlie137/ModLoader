﻿using Mod_Manager.Models;

namespace Mod_Manager.Abstractions;

internal interface ISettings
{
    public Settings.VRMode GetVRMode();
    public void SetVRMode(Settings.VRMode newValue);
}