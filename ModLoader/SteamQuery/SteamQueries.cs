﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.NetworkInformation;
using Cysharp.Threading.Tasks;
using ModLoader.Classes;
using Serilog;
using SimpleTCP;
using SteamQueries.Models;
using UnityEngine;
using Valve.Newtonsoft.Json;
using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;

namespace ModLoader.SteamQuery;

public class SteamQueries : MonoBehaviour
{
    public static SteamQueries Instance { get; private set; }
    private SimpleTcpClient _tcpClient;
    private const int _startingPort = 1234;
    private Process _process;
    private Dictionary<ulong, WorkshopDownloadQuery> _inprogressDownloads = new();
    private UniTaskCompletionSource<bool> _hasConnected = new();
        
    private async UniTaskVoid Start()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }
            
        Instance = this;
        DontDestroyOnLoad(this);
            
        var port = FindFreePort();
        var process = new ProcessStartInfo
        {
            Arguments = $"-a 3018410 -p {port}",
            FileName = "@Mod Loader\\SteamQueries\\SteamQueries.exe",
            WorkingDirectory = "@Mod Loader\\SteamQueries"
        };
        _process = Process.Start(process);
        _process.Exited += (_, _) => Debug.LogWarning("SteamQueries has closed");
        Debug.Log($"Started Steam Queries on port {port}");

        _tcpClient = new SimpleTcpClient();
        await UniTask.Delay(TimeSpan.FromSeconds(10));
            
        try
        {
            _tcpClient.DataReceived += OnDataReceived;
            _tcpClient.Connect("127.0.0.1", port);
            Debug.Log("TCP Client Connected");
            _hasConnected.TrySetResult(true);
        }
        catch (Exception e)
        {
            Debug.LogError($"Failed to connect to TCP Server {e.Message}");
            if (_process.HasExited)
            {
                Debug.LogError("The SteamQueries.exe process was closed");
            }
        }
            
    }
        
    private void OnDataReceived(object sender, Message e)
    {
        var rawMessage = e.MessageString;
        Debug.Log("Received message from steam queries");
            
        var response = default(IMessage);
        try
        {
            response = JsonConvert.DeserializeObject<IMessage>(rawMessage);
        }
        catch (Exception error)
        {
            Log.Error("Response was not JSON. {ExceptionMessage}", error.Message);
            return;
        }

        if (response is DownloadUpdate downloadUpdate && _inprogressDownloads.TryGetValue(downloadUpdate.MessageId, out var queryProgress))
        {
            queryProgress.OnProgressUpdate?.Invoke(downloadUpdate.Progress);
            return;
        }

        if (response is DownloadComplete downloadComplete && _inprogressDownloads.TryGetValue(downloadComplete.MessageId, out var queryComplete))
        {
            queryComplete.OnProgressUpdate?.Invoke(100);
            queryComplete.TaskCompletionSource.TrySetResult();
            _inprogressDownloads.Remove(downloadComplete.MessageId);
            return;
        }
    }

    private int FindFreePort(int port = _startingPort)
    {
        var isAvailable = true;
        var ipGlobalProperties = IPGlobalProperties.GetIPGlobalProperties();
        var tcpConnInfoArray = ipGlobalProperties.GetActiveTcpConnections();

        foreach (var tcpi in tcpConnInfoArray)
        {
            if (tcpi.LocalEndPoint.Port==port)
            {
                isAvailable = false;
                break;
            }
        }

        if (!isAvailable)
        {
            Debug.Log($"Failed to find free port on: {port}.");
            return FindFreePort(Random.Range(1000, 9999));
        }

            
        return port;
    }
        
    private void OnDestroy()
    {
        _tcpClient.Disconnect();
        _process.Kill();
    }

    public async UniTask<GetSubscribedItemsResponse> GetSubscribedItems(int page)
    {
        await _hasConnected.Task;
        Debug.Log($"Requesting user's subscribed items on page {page}");
        var message = new GetSubscribedItemsRequest { Page = page };
        var response = SendMessage<GetSubscribedItemsResponse>(message);
        return response;
    }

    public async UniTask<GetItemResponse> GetItem(ulong publishedFieldId)
    {
        await _hasConnected.Task;
        Debug.Log($"Requesting item details {publishedFieldId}");
        var message = new GetItemRequest { PublishFieldId = publishedFieldId };
        var response = SendMessage<GetItemResponse>(message);
        return response;
    }

    public async UniTask<LoadOnStartResponse> GetLoadOnStartSettings()
    {
        await _hasConnected.Task;
        Log.Information("Requesting Load On Start Settings");
        var response = SendMessage<LoadOnStartResponse>(new LoadOnStartRequest());
        return response;
    }

    public async UniTask DownloadItem(ulong publishFieldId)
    {
        await _hasConnected.Task;
        Log.Information("Requesting to download a workshop item");
        var response = SendMessage<DownloadResponse>(new DownloadRequest { PublishFieldId = publishFieldId });

        var taskCompletionSource = new UniTaskCompletionSource();
        _inprogressDownloads.Add(response.MessageId, new WorkshopDownloadQuery
        {
            OnProgressUpdate = amount => { Debug.Log($"Download for {publishFieldId} is at {amount}%"); },
            TaskCompletionSource = taskCompletionSource
        });

        await taskCompletionSource.Task;
    }

    private T SendMessage<T>(IMessage message)
    {
        var messageText = JsonConvert.SerializeObject(message, Formatting.None);
        var reply = _tcpClient.WriteLineAndGetReply(messageText, TimeSpan.FromSeconds(3));
            
        if (reply == null)
        {
            Log.Error("There was no response from Steam Queries for {Request}", nameof(T));
            return default;
        }
            
        var response = default(IMessage);
        try
        {
            response = JsonConvert.DeserializeObject<IMessage>(reply.MessageString);
        }
        catch (Exception e)
        {
            Log.Error("Response was not JSON. {ExceptionMessage}", e.Message);
            return default;
        }
            
        if (response is not T typeResponse)
        {
            Log.Error("Message back was not of type {TypeName}", nameof(T));
            return default;
        }

        return typeResponse;
    }

}